FROM centos:centos7

LABEL "com.example.vendor"="Talmer"

RUN yum clean all && \
    yum update -y && \
    yum -y install epel-release && \
    yum -y install PyYAML python-jinja2 python-httplib2 python-keyczar python-paramiko python-setuptools git python-pip && \
    pip install --no-cache --upgrade pip && \
    yum clean all  && \
    rm -rf /var/cache/yum

RUN mkdir /etc/ansible/
RUN echo -e '[local]\nlocalhost' > /etc/ansible/hosts
RUN pip install --no-cache ansible